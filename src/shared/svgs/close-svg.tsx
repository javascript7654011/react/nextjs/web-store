import { SvgsProps } from "../models/types";

export function CloseSvg({
  color = "#fff",
  height = "24",
  width = "24",
}: SvgsProps) {
  return (
    <svg fill={color} height={height} viewBox="0 -960 960 960" width={width}>
      <path d="m256-200-56-56 224-224-224-224 56-56 224 224 224-224 56 56-224 224 224 224-56 56-224-224-224 224Z" />
    </svg>
  );
}
