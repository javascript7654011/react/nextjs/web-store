import { SvgsProps } from "../models/types";

export function MenuSvg({
  color = "#fff",
  height = "24",
  width = "24",
}: SvgsProps) {
  return (
    <svg fill={color} height={height} width={width} viewBox="0 -960 960 960">
      <path d="M120-240v-80h720v80H120Zm0-200v-80h720v80H120Zm0-200v-80h720v80H120Z" />
    </svg>
  );
}
