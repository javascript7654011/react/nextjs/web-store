import { SvgsProps } from "../models/types";

export function ChevronRightSvg({
  color = "#fff",
  height = "24",
  width = "24",
}: SvgsProps) {
  return (
    <svg fill={color} height={height} viewBox="0 -960 960 960" width={width}>
      <path d="M504-480 320-664l56-56 240 240-240 240-56-56 184-184Z" />
    </svg>
  );
}
