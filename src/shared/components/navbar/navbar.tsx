import { ReactNode } from "react";

type NavbarProps = {
  children: ReactNode;
};

export function Navbar({ children }: NavbarProps) {
  return (
    <nav className="bg-neutral-800 flex flex-wrap py-1 px-4 w-full">
      {children}
    </nav>
  );
}
