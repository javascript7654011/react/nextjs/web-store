import { Url } from "next/dist/shared/lib/router/router";
import Link from "next/link";
import { ReactNode } from "react";

type ItemLinkProps = {
  path: Url;
  children: ReactNode;
};

export function ItemLink({ path, children }: ItemLinkProps) {
  return (
    <Link
      href={path}
      className="text-neutral-100 font-bold px-4 py-2 hover:opacity-60"
    >
      {children}
    </Link>
  );
}
