import { ReactNode } from "react";

type ChipProps = {
  children: ReactNode;
};

export function Chip({ children }: ChipProps) {
  return (
    <div className="bg-violet-500 text-white py-2 px-4 text-sm rounded-full h-fit w-fit">
      {children}
    </div>
  );
}
