import { ItemLink, Navbar } from "../navbar";

export function Header() {
  return (
    <header className="w-full">
      <Navbar>
        <ItemLink path={"/"}>Home</ItemLink>
        <p className="flex justify-end w-[calc(100vw-150px)]">
          <ItemLink path={"/auth/sign-in"}>Sign in</ItemLink>
          <ItemLink path={"/auth/sign-up"}>Sign up</ItemLink>
        </p>
      </Navbar>
    </header>
  );
}
