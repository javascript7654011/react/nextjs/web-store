"use client";
import { useRef, useState } from "react";

type TextFieldProps = {
  inputProps: React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >;
  labelName: string;
};

export function TextField({ inputProps, labelName }: TextFieldProps) {
  const inputRef = useRef<HTMLInputElement>(null);
  const [focus, setFocus] = useState<boolean>(false);
  const value = inputRef.current?.value ?? "";

  return (
    <div className="outline outline-1 outline-neutral-100 text-neutral-100 rounded relative w-fulll">
      <input
        {...inputProps}
        ref={inputRef}
        className={`bg-transparent p-2 pl-4 pr-10 text-md w-full ${inputProps.className}`}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
      />
      <label
        className={`
          absolute bg-neutral-800 text-neutral-100 px-2 duration-200  left-2 pointer-events-none
          ${focus || value.length > 0 ? "top-[-12px]" : "top-2"} 
          ${focus || value.length > 0 ? "text-sm" : "text-md"}
        `}
      >
        {labelName}
      </label>
    </div>
  );
}
