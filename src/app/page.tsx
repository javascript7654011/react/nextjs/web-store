import { ListProducts } from "@/products/components/list-products";
import { MenuAsideFilters } from "@/products/components/menu-aside-filters";
import { ProductsPagination } from "@/products/models/enums";
import { Suspense } from "react";

type HomeProps = {
  searchParams: {
    page?: string;
    records?: string;
    search?: string;
    category?: string;
  };
};

export default async function Home({ searchParams }: HomeProps) {
  const queryParams = new URLSearchParams(searchParams);
  const page = Number(queryParams.get("page") || ProductsPagination.Page);
  const records = Number(
    queryParams.get("records") || ProductsPagination.Records
  );

  return (
    <main className="flex flex-col min-h-screen w-full">
      <div className="bg-neutral-800 pb-6 mb-4 w-full">
        <MenuAsideFilters />
      </div>
      <Suspense>
        <ListProducts page={page} records={records} />
      </Suspense>
    </main>
  );
}
