import productsJson from "@/mock/products.json";
import { ProductsPagination } from "@/products/models/enums";
import { Product } from "@/products/models/types";
import { NextRequest } from "next/server";

const AllProducts = productsJson.products as Product[];

const filterByCategory = (categoryId: number) =>
  AllProducts.filter((product) => product.category.id === categoryId);

const filterByTitle = (title: string) =>
  AllProducts.filter((product) => product.title.toLowerCase().includes(title));

const filterByCategoryAndTitle = (title: string, categoryId: number) =>
  AllProducts.filter(
    (product) =>
      product.title.toLowerCase().includes(title) &&
      product.category.id === categoryId
  );

const paginateProducts = (
  products: Product[],
  page: number,
  records: number
) => {
  return {
    products: products.slice(page * records, records),
    maxPages: Math.ceil(products.length / records),
  };
};

export async function GET(request: NextRequest) {
  const queryParams = new URLSearchParams(request.nextUrl.searchParams);
  let products: Product[];
  const title = queryParams.get("search");
  const categoryId = queryParams.get("categoryId");
  const page = Number(queryParams.get("page") ?? "0");
  const records = Number(
    queryParams.get("records") ?? ProductsPagination.Records
  );

  if (title && categoryId) {
    products = filterByCategoryAndTitle(title, Number(categoryId));
  } else if (title) {
    products = filterByTitle(title);
  } else if (categoryId) {
    products = filterByCategory(Number(categoryId));
  } else {
    products = AllProducts;
  }

  return Response.json(paginateProducts(products, page, records), {
    status: 200,
  });
}
