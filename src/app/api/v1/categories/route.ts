import categoriesJson from "@/mock/categories.json";
import { Category } from "@/products/models/types";

const AllCategories = categoriesJson.categories as unknown as Category[];

export async function GET() {
  return Response.json({ categories: AllCategories }, { status: 200 });
}
