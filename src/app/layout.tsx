import { Header } from "@/shared/components/header";
import type { Metadata } from "next";
import { Roboto } from "next/font/google";
import "./globals.css";

const roboto = Roboto({
  weight: ["400", "500", "700", "900"],
  subsets: ["latin"],
});

export const metadata: Metadata = {
  title: "Ecommerce",
  description: "Store nextjs application",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="es">
      <body
        className={`${roboto.className} reserve-scroll bg-neutral-100 min-h-screen w-full max-w-screen`}
      >
        <Header />
        {children}
      </body>
    </html>
  );
}
