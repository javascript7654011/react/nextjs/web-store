import NotFoundImg from "@/assets/not-found-404.png";
import Image from "next/image";
import Link from "next/link";

export default function NotFound() {
  return (
    <main className="flex flex-col md:flex-row items-center justify-center h-[calc(100vh-200px)] max-w-full w-full">
      <div className="flex items-center justify-center pt-12 md:h-full md:w-1/3">
        <Image
          src={NotFoundImg}
          alt="nout found image"
          width={500}
          height={500}
        />
      </div>
      <div className="flex items-center justify-center flex-col pt-12 h-full w-2/3">
        <h1 className="font-black text-4xl text-violet-950">404 Not found</h1>
        <Link
          href="/"
          className="bg-violet-500 hover:bg-violet-600 text-neutral-100 py-2 px-6 rounded-lg mt-8 w-fit"
        >
          Back to home
        </Link>
      </div>
    </main>
  );
}
