export function productsFilter(
  title: string | null,
  categoryId: string | null
) {
  const filters: { title?: string; categoryId?: string } = {};
  if (title) filters.title = title;
  if (categoryId) filters.categoryId = categoryId;
  return filters;
}
