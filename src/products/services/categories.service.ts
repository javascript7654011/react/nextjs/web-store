import { Category } from "../models/types";

type GetCategoriesResponse = { categories: Category[] };

export const getCategories = async (): Promise<{ categories: Category[] }> => {
  const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}v1/categories`, {
    method: "GET",
    cache: "no-store",
  });
  const isOk = res.ok;
  const resJson = await res.json();
  if (!isOk) throw resJson;
  return resJson as GetCategoriesResponse;
};
