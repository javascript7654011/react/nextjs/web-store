import { Product } from "../models/types";

type GetProductsResponse = { products: Product[]; maxPages: number };

export const getProducts = async (
  page: number,
  records: number,
  filters: {
    title?: string;
    categoryId?: string;
  }
): Promise<GetProductsResponse> => {
  const queryParams = new URLSearchParams();
  queryParams.set("page", String(page));
  queryParams.set("records", String(records));
  if (filters.title) queryParams.set("search", filters.title);
  if (filters.categoryId) queryParams.set("categoryId", filters.categoryId);
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_API_URL}v1/products?${queryParams}`,
    {
      method: "GET",
      cache: "no-store",
    }
  );
  const isOk = res.ok;
  const resJson = await res.json();
  if (!isOk) throw resJson;
  return resJson as GetProductsResponse;
};
