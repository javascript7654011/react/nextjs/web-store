type DetailCategoryProps = {
  name: string;
};

export function DetailCategory({ name }: DetailCategoryProps) {
  return (
    <span className="bg-neutral-800 select-none font-bold py-2 px-4 text-xs text-white rounded h-fit">
      {name}
    </span>
  );
}
