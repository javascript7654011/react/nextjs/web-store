"use client";
import { ChevronRightSvg } from "@/shared/svgs";
import { Suspense, useState } from "react";
import { FilterSearch } from "../filter-search";
import { ListCategories } from "../list-categories";

export function MenuAsideFilters() {
  const [expanded, setExpanded] = useState(false);

  const handleExpanded = () => setExpanded(!expanded);

  return (
    <article className={`flex flex-col duration-500 w-full`}>
      <Suspense>
        <FilterSearch />
      </Suspense>
      <div className="pt-4 pb-2 pl-4 w-full">
        <button
          type="button"
          className={`
          ${expanded && "rotate-90"} cursor-pointer
        `}
          onClick={handleExpanded}
        >
          <ChevronRightSvg height="35" width="35" />
        </button>
      </div>
      <Suspense>{expanded && <ListCategories />}</Suspense>
    </article>
  );
}
