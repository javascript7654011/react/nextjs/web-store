"use client";
import { ShoppingCartSvg } from "@/shared/svgs";

export function BtnAddCart() {
  return (
    <button
      type="button"
      className="bg-violet-500 hover:bg-violet-600 rounded-lg px-4 py-2 h-fit w-fit"
    >
      <ShoppingCartSvg />
    </button>
  );
}
