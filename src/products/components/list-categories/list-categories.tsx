"use client";
import { useCategoriesHooks } from "@/products/hooks";
import { BtnCategory, BtnCleanCategory } from "./";

type ListCategories = {};

export function ListCategories({}: ListCategories) {
  const { categories, categoryId } = useCategoriesHooks({});

  return (
    <ul className="text-white md:flex md:flex-row md:justify-center md:items-center w-full">
      {categories.map((category) => (
        <li key={category.id} className="w-full md:w-fit">
          <BtnCategory
            id={category.id}
            name={category.name}
            categoryIdSelected={categoryId}
          />
        </li>
      ))}
      <BtnCleanCategory disabled={!categoryId} />
    </ul>
  );
}
