"use client";
import { ProductsPagination } from "@/products/models/enums";
import { MopSvg } from "@/shared/svgs";
import { usePathname, useRouter, useSearchParams } from "next/navigation";

type BtnCleanCategoryProps = {
  disabled?: boolean;
};

export function BtnCleanCategory({ disabled }: BtnCleanCategoryProps) {
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const router = useRouter();

  const removeCategory = () => {
    const queryParams = new URLSearchParams(searchParams);
    if (queryParams.get("page"))
      queryParams.set("page", ProductsPagination.Page);
    queryParams.delete("category");
    router.push(`${pathname}?${queryParams}`);
  };

  return (
    <button
      type="button"
      className="flex text-sm text-violet-500 hover:text-violet-600 disabled:text-neutral-400 font-bold py-4 ml-6 min-w-[140px]"
      disabled={disabled}
      onClick={removeCategory}
    >
      <MopSvg
        height="20"
        color={disabled ? "rgb(163,163,163)" : "rgb(139,92,246)"}
      />
      <span className="ml-2">Clean category</span>
    </button>
  );
}
