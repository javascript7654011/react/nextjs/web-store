"use client";

import { ProductsPagination } from "@/products/models/enums";
import { usePathname, useRouter, useSearchParams } from "next/navigation";

type BtnCategoryProps = {
  id: number;
  name: string;
  categoryIdSelected: string | null;
};

export function BtnCategory({
  name,
  id,
  categoryIdSelected,
}: BtnCategoryProps) {
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const router = useRouter();

  const filterByCategory = () => {
    const queryParams = new URLSearchParams(searchParams);
    if (queryParams.get("page"))
      queryParams.set("page", ProductsPagination.Page);
    queryParams.set("category", String(id));
    router.push(`${pathname}?${queryParams}`);
  };

  return (
    <button
      type="button"
      className={`
        duration-200 hover:bg-neutral-700 disabled:bg-transparent 
        disabled:text-neutral-400 py-2 px-6 text-left text-md w-full
      `}
      disabled={String(id) === categoryIdSelected}
      onClick={filterByCategory}
    >
      {name}
    </button>
  );
}
