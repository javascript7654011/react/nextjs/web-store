"use client";
import { Chip } from "@/shared/components/chip";
import { TextField } from "@/shared/components/text-field";
import { CloseSvg } from "@/shared/svgs";
import { SearchSvg } from "@/shared/svgs/search-svg";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { FormEvent, useState } from "react";

type FilterSearchProps = {};

export function FilterSearch({}: FilterSearchProps) {
  const searchParams = useSearchParams();
  const [search, setSearch] = useState(searchParams.get("search") ?? "");
  const router = useRouter();
  const pathname = usePathname();

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const queryParams = new URLSearchParams(searchParams);
    queryParams.set("search", search.trim());
    router.push(`${pathname}?${queryParams}`);
  };

  const removeSearch = () => {
    const queryParams = new URLSearchParams(searchParams);
    queryParams.delete("search");
    router.push(`${pathname}?${queryParams}`);
  };

  return (
    <form
      className="flex flex-col items-center justify-center px-4 w-full max-w-[450px]"
      onSubmit={handleSubmit}
    >
      <div className="relative py-2 w-full">
        <TextField
          inputProps={{
            name: "email",
            value: search,
            onChange: (e) => setSearch(e.target.value),
            required: true,
          }}
          labelName={"Search"}
        />
        <button type="submit" className="absolute top-[17px] right-[10px]">
          <SearchSvg height="20" width="20" />
        </button>
      </div>
      <div className="mt-2 w-full">
        {searchParams.get("search") && (
          <Chip>
            <div className="flex items-center w-full">
              <span className="mr-1">{searchParams.get("search")}</span>
              <button
                type="button"
                className="hover:opacity-50"
                onClick={removeSearch}
              >
                <CloseSvg color="#fff" width="16" height="16" />
              </button>
            </div>
          </Chip>
        )}
      </div>
    </form>
  );
}
