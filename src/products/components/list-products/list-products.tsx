"use client";
import { useProductsHooks } from "@/products/hooks";
import { CardProduct } from "../card-product";

type ListProductsProps = {
  page: number;
  records: number;
};

export function ListProducts({ page, records }: ListProductsProps) {
  const { products } = useProductsHooks({
    page,
    records,
  });

  return (
    <div className="flex flex-wrap justify-center min-h-screen w-full">
      {products.map((product) => (
        <CardProduct key={product.id} product={product} />
      ))}
    </div>
  );
}
