import { Product } from "@/products/models/types/product.type";
import Image from "next/image";
import Link from "next/link";
import { BtnAddCart } from "../btn-add-cart";
import { DetailCategory } from "../detail-category";

type CardProductProps = {
  product: Product;
};

export function CardProduct({ product }: CardProductProps) {
  return (
    <div className="bg-neutral-200 flex flex-col items-center p-4 m-4 rounded-xl h-fit w-fit max-w-[350px]">
      <Link href={`/${product.id}`}>
        <Image
          src={product.images[0]}
          className="rounded-lg h-[250px] w-[250px]"
          alt={`img-${product.title.slice(0, 5)}`}
          width={250}
          height={250}
          loading="lazy"
        />
      </Link>
      <h2 className="font-bold text-lg py-4  w-full">{product.title}</h2>
      <div className="flex items-center justify-between w-full">
        <p className="text-sm text-neutral-500 pr-4 w-3/4">
          {product.description.slice(0, 40)}
          {product.description.length > 40 && "..."}
          <Link
            href={`/${product.id}`}
            className="text-violet-500 hover:text-violet-600 font-black"
          >
            Read more
          </Link>
        </p>
        <DetailCategory name={product.category.name} />
      </div>
      <div className="flex items-center justify-between mt-6 mb-2 w-full">
        <p className="flex font-bold text-xl">
          {product.price.toLocaleString("es-AR", {
            style: "currency",
            currency: "ARS",
          })}
        </p>
        <BtnAddCart />
      </div>
    </div>
  );
}
