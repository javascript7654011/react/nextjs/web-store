/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import { useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import { Product } from "../models/types";
import { getProducts } from "../services";
import { productsFilter } from "../utils";

type UseProductsHooksProps = {
  page: number;
  records: number;
};

export function useProductsHooks({ page, records }: UseProductsHooksProps) {
  const [products, setProducts] = useState<Product[]>([]);
  const [maxPage, setMaxPage] = useState<number>();
  const searchParams = useSearchParams();

  const actionProducts = () => {
    const filters = productsFilter(
      searchParams.get("search"),
      searchParams.get("category")
    );
    getProducts(page, records, filters).then(({ maxPages, products }) => {
      setProducts(products);
      setMaxPage(maxPages);
    });
  };

  useEffect(() => {
    actionProducts();
  }, [searchParams]);

  return { products, maxPage };
}
