/* eslint-disable react-hooks/exhaustive-deps */
"use client";
import { useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import { Category } from "../models/types";
import { getCategories } from "../services";

type UseCategoriesHooksProps = {};

export function useCategoriesHooks({}: UseCategoriesHooksProps) {
  const searchParams = useSearchParams();
  const [categories, setCategories] = useState<Category[]>([]);

  const actionProducts = () => {
    getCategories().then(({ categories }) => {
      setCategories(categories);
    });
  };

  useEffect(() => {
    actionProducts();
  }, [searchParams.get("category")]);

  return { categories, categoryId: searchParams.get("category") };
}
